""" 
The purpose of this script is to list all tags published by the OPC-UA
server.

The purpose of this script is to list all tags published by the OPC-UA
server.

Requirements:
    OPCUA Server running on the PAC side of the CPL-410

Parameters: 
    none

Output: 
    Returns a list of all published tags.
"""

from opcua import Client, ua

myClient = Client("opc.tcp://localControllerHost:4840")
try:
    myClient.connect()
    myClient.load_type_definitions()
    variablesnode = myClient.get_node(ua.NodeId(1001, 2)) #This is the root node containing all of the published tags of the connected CPL410
    variables = variablesnode.get_children() #variables is now a list of all published tags

    #print("Root node is: ", variablesnode) #Uncomment to see the root node
    #print("Objects node is: ", variables) #Uncomment to see more detailed information on each OPC-UA object
    for child in variables:
        print(child.get_browse_name().Name)

finally:
    myClient.disconnect()
    print("Disconnected!!!")
