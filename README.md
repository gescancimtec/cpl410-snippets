# This is a repository for interesting little pieces of code written for the CPL410. Snippets may be useful for learning or for incorporating into other projects. Basically, if you write a small dedicated piece of code that is useful on its own, but not a complete application, put it here!

---

## All code snippets in this section, should be:

1. Well documented so that a *programmer* can see what is going on.
2. If it's a Python3 file, the docstring (i.e. triple quotes **'''** or **"""**) should include assumptions, inputs (if any), and outputs.
3. Outputs should result in something to the console and/or where to access ouput (i.e. a backend database, Grafana, etc.)
4. Run on their own (i.e. you can python3 *filename.py* and see results.  Write a main wrapper if you have to.
5. DO ONE THING, or at least something simple.  If the code does a lot of stuff, its probably a full blown app and should be in the master/development branch.
6. In their own directory that also contains any necessary support files (i.e. shell scripts, jpgs, json files, Proficy Machine Edition projects, etc.)
7. Did I mention well documented?  Include a *readme* if that is what is necessary.
---

## Example Python3 Docstring/Code format:
>
>		def my_cool_function: 
>   		""" 
>   		my_cool_function returns a dictionary of all visible OPC-UA tags that it can see (i.e. published tags from a PME project)
>  
>   		The purpose of this script is to return a Python3 dictionary of OPCUA tags from a PME project that have their published
>			value set to 'External Read Only' or 'External Read/Write'.  This dictionary consists of the name of the tag as a key and the 
>			UA node id of the tag as its value.  
>
>			Requirements:
>			Any PME project with at least one published tag (no sample provided)
>  	
>    		Parameters: 
>    		none
>  	
>    		Returns: 
>    		dict: 	key: Name of tag as String
>					value: UA Node Id 
>  	
>    		"""
>		
>			#Code goes here
>  	
>    		return myDict
>  	
>		print my_cool_function
