""" 
The purpose of this script is to test the number of concurrent OPCUA sessions
that the CPL410 can support.  

This is a command line script that expects you to provide a maxsessions
value in the script.  It then attempts to open that number of sessions.
When that number is reached (or the script fails because it cannot open
that many sessions), each session is closed and the program exits.

Requirements:
    OPC-UA Server must be enabled on the CPL410 PAC Engine side.

Parameters: 
    none

Output: 
    Prints session information
"""
from opcua import Client, ua

sessionlist = []
maxsessions = 11
try:
    for t in range(maxsessions):
        myClient = Client("opc.tcp://192.168.180.2:4840")
        sessionlist.append(myClient)
        sessionlist[t].connect()
        print(t, sessionlist[t].get_server_node())

    for session in sessionlist:
        print(session)

finally:
    print("Closing Sessions...")        
    for t in range(maxsessions):
        print("Closing session ",t)
        sessionlist[t].disconnect()
   