""" 
The purpose of this script is to show the absolute basics of interacting with
free opcua on the CPL410.

Use the PME project provided with this script to read a celsius value being
generated from the PLC side of the CPL410, perform a simple temperature
conversion, and pass the value back to the CPL410.  Also, print both values to
the Linux console.

Requirements:
    Use the supplied PME file so tag names match what is expected. 

Parameters: 
    none

Output: 
    Returns a Fahrenheit value to the OPCUA Server and prints Celsius and
    Fahrenheit values to the console.
"""

from opcua import Client, ua
from sys import exit

# The following line uses the IP address reserved for the internal
# NIC connection to pass OPC-UA variables.
myClient = Client("opc.tcp://localControllerHost:4840")
myClient.connect()
# Here are the two variables of interest from our PME project.  They are
# in OPCUA namespace 2.
celsius = myClient.get_node("ns=2;s=Celsius")
fahrenheit = myClient.get_node("ns=2;s=PythonFahrenheit")

while(True):
    try:
        # Now we will get the values from one variable and set the other
        celsiusTemp = localInt.get_value()
        fahrenheit.set_value(int(celsiusTemp * 9/5 + 32),ua.VariantType.Int16)
        # Print the values to the local console
        print(celsiusTemp, " | " ,fahrenheit.get_value())
    except:
        # And finally close our connection if user uses ctrl-c to exit
        myClient.disconnect()
        print("Connection closed")
        exit()
