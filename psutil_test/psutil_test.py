""" 
The purpose of this script is to obtain IP Address information for the EFA
port

Uses the psutil library to obtain information about the IP address

Requirements:
    must "python3 -m pip install psutil" prior to running this script the
    first time

Parameters: 
    none

Output: 
    IP information for the EFA port
"""
import psutil

for nic, addrs in psutil.net_if_addrs().items():
    if(nic == 'enp1s0'):
        print("%s:" % (nic))
        for addr in addrs:
            if(addr.family == 2):
                print(" address   : %s" % addr.address)
                print("         netmask   : %s" % addr.netmask)
                
mycard = psutil.net_if_addrs()['enp1s0']
print(mycard)
for addr in mycard:
    if(addr.family == 2):
        print(" address   : %s" % addr.address)
        print("         netmask   : %s" % addr.netmask)