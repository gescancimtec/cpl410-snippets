""" 
The purpose of this script is to take a picture with a USB connected camera
using Open CV.

Everytime this script is run, a picture should be taken by the last connected
camera to the CPL410

Requirements:
    In order for this script to work, several libraries must be installed.  
    Run the following from the command line once:
        1) sudo apt-get install -y libsm6 libxext6 libxrender-dev
        2) python3 -m pip install opencv-python

Parameters: 
    none

Output: 
    Saves a picture with the filename 'opencv1.png' to the same directory
    as the script.
"""
import cv2

#Create the camera object and set resolution.  Get an image from the camera to work with
camera = cv2.VideoCapture(-1)
camera.set(3,800)
camera.set(4,600)
ret, image = camera.read()
cv2.imwrite('opencv1.png', image)
del(camera)