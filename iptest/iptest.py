""" 
The purpose of this script is to obtain IP Address information for the EFA
port

Uses the socket library to obtain information about the IP address.  Check
out the psutil_test script for another (less hacky) way to get this
information.

Requirements:
    none

Parameters: 
    none

Output: 
    IP information for the EFA port
"""

import socket
def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

print(get_ip())